﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace TestTask.DynamicLabs
{
	class Program
	{
		private static readonly string ZipPath = GetDefaultPath("ZIPPY.zip"); // <-- comment these two field if you don't want to place and create file in your root folder.
		private static readonly string ExtractPath = GetDefaultPath("TempFolder");
		static void Main(string[] args)
		{
			var processor = new ZipProcessorEx(ZipPath, ExtractPath);
			var result = processor.ProcessArchiveAsync(MethodB);
			Console.WriteLine("Program was finished with result of {0}", result.Result);
			Console.ReadLine();
		}
		/// <summary>
		/// Test method
		/// </summary>
		/// <param name="file">file to process</param>
		private static void MethodB(FileInfo file)
		{
			Console.WriteLine("method B was called upon {0}", file.FullName);
		}
		/// <summary>
		/// Combines root path with folder or file name.
		/// </summary>
		/// <param name="endpoint">folder or file name</param>
		/// <returns></returns>
		private static string GetDefaultPath(string endpoint)
		{
			return Path.Combine(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)), endpoint);
		}
	}
}
