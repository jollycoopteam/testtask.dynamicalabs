﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.DynamicLabs
{
	public enum OperationStatus
	{
		Done = 0,
		ArchiveUnavailable = 1,
		UnexpectedTermination = 2
	}
}
