﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.DynamicLabs
{
	public class ZipProcessorEx
	{
		#region	private Fields
		/// <summary>
		/// Default Folder to create.
		/// </summary>
		private const string FolderName = "Zip_TempFolder_0";
		#endregion
		#region Properties

		private string ArchivePath { get; }
		private string FolderPath { get; }

		#endregion
		#region Constructors
		public ZipProcessorEx(string archivePath)
		{
			ArchivePath = archivePath;
			FolderPath = GetDefaultPath();
		}

		public ZipProcessorEx(string archivePath, string folderPath)
		{
			ArchivePath = archivePath;
			FolderPath = folderPath;
		}
		#endregion

		#region Methods
		/// <summary>
		/// Sync wrapper method, that wrapps main logic.
		/// </summary>
		/// <param name="method">method to call upon each extracting file</param>
		/// <returns></returns>
		public OperationStatus ProcessArchive(Action<FileInfo> method)
		{
			return Valid(ArchivePath) ? ProcessArchiveWith(method) : OperationStatus.ArchiveUnavailable;
		}
		/// <summary>
		/// Async wrapper method, that wrapps main logic.
		/// </summary>
		/// <param name="method">method to call upon each extracting file</param>
		/// <returns></returns>
		public Task<OperationStatus> ProcessArchiveAsync(Action<FileInfo> method)
		{
			return Task.FromResult(Valid(ArchivePath) ? ProcessArchiveWith(method) : OperationStatus.ArchiveUnavailable);
		}
		#endregion

		#region Private
		private OperationStatus ProcessArchiveWith(Action<FileInfo> method)
		{
			try
			{
				var folder = Directory.CreateDirectory(FolderPath);
				using (var archive = ZipFile.Open(ArchivePath, ZipArchiveMode.Read))
				{
					foreach (var entry in archive.Entries)
					{
						ExtractEntry(entry);
					}
				}
				var files = folder.EnumerateFiles();
				files.AsParallel().AsOrdered().ForAll(method);
				folder.Delete(true);
				return OperationStatus.Done;
			}
			catch (Exception)
			{
				return OperationStatus.UnexpectedTermination;
			}
		}
		/// <summary>
		/// Extracts file from archive if condition is met.
		/// </summary>
		/// <param name="entry">archive entry</param>
		private void ExtractEntry(ZipArchiveEntry entry)
		{
			if (entry.FullName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
			{
				entry.ExtractToFile(Path.Combine(FolderPath, entry.Name));
			}
		}

		private string GetDefaultPath()
		{
			return Path.Combine(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)), FolderName);
		}
		/// <summary>
		/// Check if file referenced by specific path exists.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		private bool Valid(string path)
		{
			return File.Exists(path);
		}
		#endregion
	}
}
